﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace Graph2TeX
{
    public class LineData
    {
        public event EventHandler PropertyChanged;
        float _Thickness;
        public float Thickness
        {
            get
            {
                return _Thickness;
            }
            set
            {
                if (_Thickness == value)
                {
                    return;
                }

                _Thickness = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        LineType _lineType;
        public LineType lineType 
        { 
            get
            {
                return _lineType;
            }
            set
            {
                if (_lineType == value)
                {
                    return;
                }

                _lineType = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        public Text Title 
        { 
            get;
            set;
        }

        public PointF[] RawData { get; set; }

        public LineData(PointF[] rawData)
        {
            RawData = rawData;
            Thickness = 1.0F;
            lineType = LineType.Solid;
            Title = new Text("", 8.0F);
        }
    }
}
