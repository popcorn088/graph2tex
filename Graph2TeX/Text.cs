﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graph2TeX
{
    public class Text
    {
        public event EventHandler PropertyChanged;

        /// <summary>
        /// 文字列
        /// </summary>
        string _text;
        public string text
        {
            get
            {
                return _text;
            }
            set
            {
                if (string.Equals(value, _text) == true)
                {
                    return;
                }

                _text = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }
        /// <summary>
        /// フォントサイズ[pt]
        /// </summary>
        float _size_pt;
        public float size_pt
        {
            get
            {
                return _size_pt;
            }
            set
            {
                if (_size_pt == value)
                {
                    return;
                }

                _size_pt = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }

        }
        /// <summary>
        /// 表示位置[mm]
        /// </summary>
        public PointF pos_mm { get; set; }
        /// <summary>
        /// アライメント
        /// </summary>
        StringFormat string_format { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="_text"></param>
        /// <param name="_size_pt"></param>
        public Text(string _text, float _size_pt)
        {
            text = _text;
            size_pt = _size_pt;
            pos_mm = new PointF();
            // 矩形中央・折り返しなし
            string_format = new StringFormat();
            string_format.Alignment = StringAlignment.Center;
            string_format.LineAlignment = StringAlignment.Center;
            string_format.FormatFlags = StringFormatFlags.NoWrap;
        }
        /// <summary>
        /// コピーコンストラクタ
        /// </summary>
        /// <param name="source">コピー元</param>
        public Text(Text source)
        {
            text = source.text;
            size_pt = source.size_pt;
            pos_mm = source.pos_mm;
            string_format = source.string_format;
        }
    }
}
