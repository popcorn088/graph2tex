﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graph2TeX
{
    class TeXLib
    {
        public static string BeginPicture(SizeF Drawmm)
        {
            string res = @"{\unitlength 1in%" + Environment.NewLine;
            SizeF Drawinch = ViewData.mmToinch(Drawmm);
            res += @"\begin{picture}(" + Drawinch.Width.ToString("F4") + ", " + Drawinch.Height.ToString("F4") + @")(0,0)%" + Environment.NewLine;
            return res;

        }

        public static string EndPicture()
        {
            return @"\end{picture}}%" + Environment.NewLine;
        }

        public static string Line(LineType lineType, float thickness, PointF beg_pos, PointF end_pos)
        {
            string ret = "";
            ret += @"\special{pn " + ViewData.ptTominch(thickness).ToString("F4") + @"}%" + Environment.NewLine;
            PointF beg_pos_inch = ViewData.mmToinch(beg_pos);
            beg_pos_inch.X *= 1000;
            beg_pos_inch.Y *= -1000;
            ret += @"\special{pa " + beg_pos_inch.X.ToString("F4") + " " + beg_pos_inch.Y.ToString("F4") + @"}%" + Environment.NewLine;
            PointF end_pos_inch = ViewData.mmToinch(end_pos);
            end_pos_inch.X *= 1000;
            end_pos_inch.Y *= -1000;
            ret += @"\special{pa " + end_pos_inch.X.ToString("F4") + " " + end_pos_inch.Y.ToString("F4") + @"}%" + Environment.NewLine;
            switch (lineType)
            {
                case LineType.Dash:
                    ret += @"\special{da 0.070}%" + Environment.NewLine;
                    break;
                case LineType.Dot:
                    ret += @"\special{dt 0.045}%" + Environment.NewLine;
                    break;
                default:
                    break;
            }
            ret += @"\special{fp}%" + Environment.NewLine;

            return ret;
        }

        public static string Line(LineType lineType, float thickness_pt, PointF[] datamm)
        {
            string ret = "";
            ret += @"\special{pn " + ViewData.ptTominch(thickness_pt).ToString("F4") + @"}%" + Environment.NewLine;
            PointF[] datainch = new PointF[datamm.Length];
            for (int i = 0; i < datainch.Length; i++)
            {
                PointF data_i = ViewData.mmToinch(datamm[i]);
                float x = data_i.X * 1000;
                float y = data_i.Y * -1000;
                datainch[i] = new PointF(x, y);
            }

            for (int i = 0; i < datainch.Length; i++)
            {
                ret += @"\special{pa " + datainch[i].X.ToString("F4") + " " + datainch[i].Y.ToString("F4") + @"}%" + Environment.NewLine;
            }
            switch (lineType)
            {
                case LineType.Dash:
                    ret += @"\special{da 0.070}%" + Environment.NewLine;
                    break;
                case LineType.Dot:
                    ret += @"\special{dt 0.045}%" + Environment.NewLine;
                    break;
                default:
                    break;
            }
            ret += @"\special{fp}%" + Environment.NewLine;

            return ret;
        }

        public static string Text(string text, float font_size_pt, Alignment alignment, LineAlignment lineAlignment, PointF position_mm)
        {
            return Text(text, font_size_pt, alignment, lineAlignment, 0, Alignment.Center, position_mm);
        }

        public static string Text(string text, float font_size_pt, Alignment alignment, LineAlignment lineAlignment, float rotation_degree, Alignment rotationAlignment, PointF position_mm)
        {
            PointF pos_inch = ViewData.mmToinch(position_mm);
            string ret = @"\put(" + pos_inch.X.ToString("F4") + "," + pos_inch.Y.ToString("F4") + @"){\makebox(0,0)[";
            switch (alignment)
            {
                case Alignment.Center:
                    ret += "c";
                    break;
                case Alignment.Right:
                    ret += "r";
                    break;
                default:
                    ret += "l";
                    break;
            }
            switch (lineAlignment)
            {
                case LineAlignment.Top:
                    ret += "t";
                    break;
                case LineAlignment.Bottom:
                    ret += "b";
                    break;
                default:
                    ret += "c";
                    break;
            }
            ret += @"]{\rotatebox[origin=";
            switch (rotationAlignment)
            {
                case Alignment.Center:
                    ret += "c";
                    break;
                case Alignment.Right:
                    ret += "r";
                    break;
                default:
                    ret += "l";
                    break;
            }
            ret += "]{" + rotation_degree.ToString() + @"}{{\fontsize{" + font_size_pt.ToString() + "pt" + "}" + "{" + font_size_pt + "pt" + @"}\selectfont{" + text + @"}}}}}%" + Environment.NewLine;

            return ret;
        }
    }
}
