﻿
namespace Graph2TeX
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pb = new System.Windows.Forms.PictureBox();
            this.nudWidth = new System.Windows.Forms.NumericUpDown();
            this.nudHeight = new System.Windows.Forms.NumericUpDown();
            this.nudOriginY = new System.Windows.Forms.NumericUpDown();
            this.nudOriginX = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nudDiagonalY = new System.Windows.Forms.NumericUpDown();
            this.nudDiagonalX = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nudXAxisThickness = new System.Windows.Forms.NumericUpDown();
            this.nudYAxisThickness = new System.Windows.Forms.NumericUpDown();
            this.nudYDistance = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.nudXDistance = new System.Windows.Forms.NumericUpDown();
            this.nudYSubThickness = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.nudXSubThickness = new System.Windows.Forms.NumericUpDown();
            this.nudYAxisMax = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudXAxisMax = new System.Windows.Forms.NumericUpDown();
            this.nudYAxisMin = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.nudXAxisMin = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.nudYDigit = new System.Windows.Forms.NumericUpDown();
            this.nudXDigit = new System.Windows.Forms.NumericUpDown();
            this.cmbMainAxisX = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbMainAxisY = new System.Windows.Forms.ComboBox();
            this.cmbSubAxisY = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cmbSubAxisX = new System.Windows.Forms.ComboBox();
            this.nudYNotchedFontSize = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.nudXNotchedFontSize = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.tbTitleText = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.nudTitleFontSize = new System.Windows.Forms.NumericUpDown();
            this.tbXAxisTitle = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.nudXAxisTitleFontSize = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.nudYAxisTitleFontSize = new System.Windows.Forms.NumericUpDown();
            this.tbYAxisTitle = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.flp = new System.Windows.Forms.FlowLayoutPanel();
            this.cbLegendVisible = new System.Windows.Forms.CheckBox();
            this.btTeX = new System.Windows.Forms.Button();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.cmbGraphType = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOriginY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOriginX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiagonalY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiagonalX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXDistance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYSubThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXSubThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYDigit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXDigit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYNotchedFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXNotchedFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTitleFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisTitleFontSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisTitleFontSize)).BeginInit();
            this.flp.SuspendLayout();
            this.SuspendLayout();
            // 
            // pb
            // 
            this.pb.BackColor = System.Drawing.SystemColors.Window;
            this.pb.Location = new System.Drawing.Point(12, 12);
            this.pb.Name = "pb";
            this.pb.Size = new System.Drawing.Size(500, 500);
            this.pb.TabIndex = 0;
            this.pb.TabStop = false;
            // 
            // nudWidth
            // 
            this.nudWidth.Location = new System.Drawing.Point(628, 90);
            this.nudWidth.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudWidth.Name = "nudWidth";
            this.nudWidth.Size = new System.Drawing.Size(59, 19);
            this.nudWidth.TabIndex = 2;
            this.nudWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudHeight
            // 
            this.nudHeight.Location = new System.Drawing.Point(693, 90);
            this.nudHeight.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudHeight.Name = "nudHeight";
            this.nudHeight.Size = new System.Drawing.Size(59, 19);
            this.nudHeight.TabIndex = 3;
            this.nudHeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudOriginY
            // 
            this.nudOriginY.Location = new System.Drawing.Point(693, 115);
            this.nudOriginY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudOriginY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudOriginY.Name = "nudOriginY";
            this.nudOriginY.Size = new System.Drawing.Size(59, 19);
            this.nudOriginY.TabIndex = 5;
            this.nudOriginY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudOriginX
            // 
            this.nudOriginX.Location = new System.Drawing.Point(628, 115);
            this.nudOriginX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudOriginX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudOriginX.Name = "nudOriginX";
            this.nudOriginX.Size = new System.Drawing.Size(59, 19);
            this.nudOriginX.TabIndex = 4;
            this.nudOriginX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(520, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "DrawSize";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(520, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "Origin";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(520, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 12);
            this.label3.TabIndex = 10;
            this.label3.Text = "Diagonal";
            // 
            // nudDiagonalY
            // 
            this.nudDiagonalY.Location = new System.Drawing.Point(693, 140);
            this.nudDiagonalY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudDiagonalY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDiagonalY.Name = "nudDiagonalY";
            this.nudDiagonalY.Size = new System.Drawing.Size(59, 19);
            this.nudDiagonalY.TabIndex = 9;
            this.nudDiagonalY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudDiagonalX
            // 
            this.nudDiagonalX.Location = new System.Drawing.Point(628, 140);
            this.nudDiagonalX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudDiagonalX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudDiagonalX.Name = "nudDiagonalX";
            this.nudDiagonalX.Size = new System.Drawing.Size(59, 19);
            this.nudDiagonalX.TabIndex = 8;
            this.nudDiagonalX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(520, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 12);
            this.label4.TabIndex = 12;
            this.label4.Text = "Axis.Thickness";
            // 
            // nudXAxisThickness
            // 
            this.nudXAxisThickness.Location = new System.Drawing.Point(628, 165);
            this.nudXAxisThickness.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudXAxisThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudXAxisThickness.Name = "nudXAxisThickness";
            this.nudXAxisThickness.Size = new System.Drawing.Size(59, 19);
            this.nudXAxisThickness.TabIndex = 11;
            this.nudXAxisThickness.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudYAxisThickness
            // 
            this.nudYAxisThickness.Location = new System.Drawing.Point(693, 165);
            this.nudYAxisThickness.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudYAxisThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudYAxisThickness.Name = "nudYAxisThickness";
            this.nudYAxisThickness.Size = new System.Drawing.Size(59, 19);
            this.nudYAxisThickness.TabIndex = 13;
            this.nudYAxisThickness.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudYDistance
            // 
            this.nudYDistance.DecimalPlaces = 2;
            this.nudYDistance.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudYDistance.Location = new System.Drawing.Point(693, 190);
            this.nudYDistance.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudYDistance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudYDistance.Name = "nudYDistance";
            this.nudYDistance.Size = new System.Drawing.Size(59, 19);
            this.nudYDistance.TabIndex = 16;
            this.nudYDistance.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(520, 191);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "Axis.Distance";
            // 
            // nudXDistance
            // 
            this.nudXDistance.DecimalPlaces = 2;
            this.nudXDistance.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudXDistance.Location = new System.Drawing.Point(628, 190);
            this.nudXDistance.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudXDistance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudXDistance.Name = "nudXDistance";
            this.nudXDistance.Size = new System.Drawing.Size(59, 19);
            this.nudXDistance.TabIndex = 14;
            this.nudXDistance.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudYSubThickness
            // 
            this.nudYSubThickness.Location = new System.Drawing.Point(693, 341);
            this.nudYSubThickness.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudYSubThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudYSubThickness.Name = "nudYSubThickness";
            this.nudYSubThickness.Size = new System.Drawing.Size(59, 19);
            this.nudYSubThickness.TabIndex = 19;
            this.nudYSubThickness.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(520, 342);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(78, 12);
            this.label6.TabIndex = 18;
            this.label6.Text = "Sub.Thickness";
            // 
            // nudXSubThickness
            // 
            this.nudXSubThickness.Location = new System.Drawing.Point(628, 341);
            this.nudXSubThickness.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudXSubThickness.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudXSubThickness.Name = "nudXSubThickness";
            this.nudXSubThickness.Size = new System.Drawing.Size(59, 19);
            this.nudXSubThickness.TabIndex = 17;
            this.nudXSubThickness.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudYAxisMax
            // 
            this.nudYAxisMax.DecimalPlaces = 2;
            this.nudYAxisMax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudYAxisMax.Location = new System.Drawing.Point(693, 215);
            this.nudYAxisMax.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudYAxisMax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudYAxisMax.Name = "nudYAxisMax";
            this.nudYAxisMax.Size = new System.Drawing.Size(59, 19);
            this.nudYAxisMax.TabIndex = 22;
            this.nudYAxisMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(520, 216);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "Axis.Max";
            // 
            // nudXAxisMax
            // 
            this.nudXAxisMax.DecimalPlaces = 2;
            this.nudXAxisMax.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudXAxisMax.Location = new System.Drawing.Point(628, 215);
            this.nudXAxisMax.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudXAxisMax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudXAxisMax.Name = "nudXAxisMax";
            this.nudXAxisMax.Size = new System.Drawing.Size(59, 19);
            this.nudXAxisMax.TabIndex = 20;
            this.nudXAxisMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudYAxisMin
            // 
            this.nudYAxisMin.DecimalPlaces = 2;
            this.nudYAxisMin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudYAxisMin.Location = new System.Drawing.Point(693, 240);
            this.nudYAxisMin.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudYAxisMin.Name = "nudYAxisMin";
            this.nudYAxisMin.Size = new System.Drawing.Size(59, 19);
            this.nudYAxisMin.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(520, 241);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 12);
            this.label8.TabIndex = 24;
            this.label8.Text = "Axis.Min";
            // 
            // nudXAxisMin
            // 
            this.nudXAxisMin.DecimalPlaces = 2;
            this.nudXAxisMin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.nudXAxisMin.Location = new System.Drawing.Point(628, 240);
            this.nudXAxisMin.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudXAxisMin.Name = "nudXAxisMin";
            this.nudXAxisMin.Size = new System.Drawing.Size(59, 19);
            this.nudXAxisMin.TabIndex = 23;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(520, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 12);
            this.label9.TabIndex = 28;
            this.label9.Text = "Digit";
            // 
            // nudYDigit
            // 
            this.nudYDigit.Location = new System.Drawing.Point(693, 265);
            this.nudYDigit.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudYDigit.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudYDigit.Name = "nudYDigit";
            this.nudYDigit.Size = new System.Drawing.Size(59, 19);
            this.nudYDigit.TabIndex = 27;
            this.nudYDigit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudXDigit
            // 
            this.nudXDigit.Location = new System.Drawing.Point(628, 265);
            this.nudXDigit.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudXDigit.Name = "nudXDigit";
            this.nudXDigit.Size = new System.Drawing.Size(59, 19);
            this.nudXDigit.TabIndex = 26;
            this.nudXDigit.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cmbMainAxisX
            // 
            this.cmbMainAxisX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMainAxisX.FormattingEnabled = true;
            this.cmbMainAxisX.Location = new System.Drawing.Point(628, 290);
            this.cmbMainAxisX.Name = "cmbMainAxisX";
            this.cmbMainAxisX.Size = new System.Drawing.Size(59, 20);
            this.cmbMainAxisX.TabIndex = 29;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(520, 292);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 12);
            this.label10.TabIndex = 30;
            this.label10.Text = "Axis.LineType";
            // 
            // cmbMainAxisY
            // 
            this.cmbMainAxisY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMainAxisY.FormattingEnabled = true;
            this.cmbMainAxisY.Location = new System.Drawing.Point(693, 290);
            this.cmbMainAxisY.Name = "cmbMainAxisY";
            this.cmbMainAxisY.Size = new System.Drawing.Size(59, 20);
            this.cmbMainAxisY.TabIndex = 31;
            // 
            // cmbSubAxisY
            // 
            this.cmbSubAxisY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubAxisY.FormattingEnabled = true;
            this.cmbSubAxisY.Location = new System.Drawing.Point(693, 366);
            this.cmbSubAxisY.Name = "cmbSubAxisY";
            this.cmbSubAxisY.Size = new System.Drawing.Size(59, 20);
            this.cmbSubAxisY.TabIndex = 34;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(520, 368);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(95, 12);
            this.label11.TabIndex = 33;
            this.label11.Text = "SubAxis.LineType";
            // 
            // cmbSubAxisX
            // 
            this.cmbSubAxisX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSubAxisX.FormattingEnabled = true;
            this.cmbSubAxisX.Location = new System.Drawing.Point(628, 366);
            this.cmbSubAxisX.Name = "cmbSubAxisX";
            this.cmbSubAxisX.Size = new System.Drawing.Size(59, 20);
            this.cmbSubAxisX.TabIndex = 32;
            // 
            // nudYNotchedFontSize
            // 
            this.nudYNotchedFontSize.Location = new System.Drawing.Point(693, 316);
            this.nudYNotchedFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudYNotchedFontSize.Name = "nudYNotchedFontSize";
            this.nudYNotchedFontSize.Size = new System.Drawing.Size(59, 19);
            this.nudYNotchedFontSize.TabIndex = 37;
            this.nudYNotchedFontSize.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(520, 317);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(99, 12);
            this.label12.TabIndex = 36;
            this.label12.Text = "Notched Font Size";
            // 
            // nudXNotchedFontSize
            // 
            this.nudXNotchedFontSize.Location = new System.Drawing.Point(628, 316);
            this.nudXNotchedFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudXNotchedFontSize.Name = "nudXNotchedFontSize";
            this.nudXNotchedFontSize.Size = new System.Drawing.Size(59, 19);
            this.nudXNotchedFontSize.TabIndex = 35;
            this.nudXNotchedFontSize.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(520, 40);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(50, 12);
            this.label13.TabIndex = 38;
            this.label13.Text = "Title.text";
            // 
            // tbTitleText
            // 
            this.tbTitleText.Location = new System.Drawing.Point(628, 40);
            this.tbTitleText.Name = "tbTitleText";
            this.tbTitleText.Size = new System.Drawing.Size(124, 19);
            this.tbTitleText.TabIndex = 39;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(520, 67);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(64, 12);
            this.label14.TabIndex = 41;
            this.label14.Text = "Title.size_pt";
            // 
            // nudTitleFontSize
            // 
            this.nudTitleFontSize.Location = new System.Drawing.Point(628, 65);
            this.nudTitleFontSize.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudTitleFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudTitleFontSize.Name = "nudTitleFontSize";
            this.nudTitleFontSize.Size = new System.Drawing.Size(59, 19);
            this.nudTitleFontSize.TabIndex = 40;
            this.nudTitleFontSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tbXAxisTitle
            // 
            this.tbXAxisTitle.Location = new System.Drawing.Point(628, 392);
            this.tbXAxisTitle.Name = "tbXAxisTitle";
            this.tbXAxisTitle.Size = new System.Drawing.Size(124, 19);
            this.tbXAxisTitle.TabIndex = 43;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(520, 395);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 12);
            this.label15.TabIndex = 42;
            this.label15.Text = "XAxis.Title.text";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(520, 419);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(96, 12);
            this.label16.TabIndex = 45;
            this.label16.Text = "XAxis.Title.size_pt";
            // 
            // nudXAxisTitleFontSize
            // 
            this.nudXAxisTitleFontSize.Location = new System.Drawing.Point(628, 417);
            this.nudXAxisTitleFontSize.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudXAxisTitleFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudXAxisTitleFontSize.Name = "nudXAxisTitleFontSize";
            this.nudXAxisTitleFontSize.Size = new System.Drawing.Size(59, 19);
            this.nudXAxisTitleFontSize.TabIndex = 44;
            this.nudXAxisTitleFontSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(520, 469);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 12);
            this.label17.TabIndex = 49;
            this.label17.Text = "YAxis.Title.size_pt";
            // 
            // nudYAxisTitleFontSize
            // 
            this.nudYAxisTitleFontSize.Location = new System.Drawing.Point(628, 467);
            this.nudYAxisTitleFontSize.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.nudYAxisTitleFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudYAxisTitleFontSize.Name = "nudYAxisTitleFontSize";
            this.nudYAxisTitleFontSize.Size = new System.Drawing.Size(59, 19);
            this.nudYAxisTitleFontSize.TabIndex = 48;
            this.nudYAxisTitleFontSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tbYAxisTitle
            // 
            this.tbYAxisTitle.Location = new System.Drawing.Point(628, 442);
            this.tbYAxisTitle.Name = "tbYAxisTitle";
            this.tbYAxisTitle.Size = new System.Drawing.Size(124, 19);
            this.tbYAxisTitle.TabIndex = 47;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(520, 445);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 12);
            this.label18.TabIndex = 46;
            this.label18.Text = "YAxis.Title.text";
            // 
            // flp
            // 
            this.flp.Controls.Add(this.cbLegendVisible);
            this.flp.Dock = System.Windows.Forms.DockStyle.Right;
            this.flp.Location = new System.Drawing.Point(758, 0);
            this.flp.Name = "flp";
            this.flp.Size = new System.Drawing.Size(186, 531);
            this.flp.TabIndex = 50;
            // 
            // cbLegendVisible
            // 
            this.cbLegendVisible.AutoSize = true;
            this.cbLegendVisible.Checked = true;
            this.cbLegendVisible.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbLegendVisible.Location = new System.Drawing.Point(3, 3);
            this.cbLegendVisible.Name = "cbLegendVisible";
            this.cbLegendVisible.Size = new System.Drawing.Size(59, 16);
            this.cbLegendVisible.TabIndex = 0;
            this.cbLegendVisible.Text = "Visible";
            this.cbLegendVisible.UseVisualStyleBackColor = true;
            // 
            // btTeX
            // 
            this.btTeX.Location = new System.Drawing.Point(677, 489);
            this.btTeX.Name = "btTeX";
            this.btTeX.Size = new System.Drawing.Size(75, 23);
            this.btTeX.TabIndex = 51;
            this.btTeX.Text = "TeX";
            this.btTeX.UseVisualStyleBackColor = true;
            this.btTeX.Click += new System.EventHandler(this.btTeX_Click);
            // 
            // cmbGraphType
            // 
            this.cmbGraphType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGraphType.FormattingEnabled = true;
            this.cmbGraphType.Location = new System.Drawing.Point(628, 14);
            this.cmbGraphType.Name = "cmbGraphType";
            this.cmbGraphType.Size = new System.Drawing.Size(121, 20);
            this.cmbGraphType.TabIndex = 52;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(520, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 12);
            this.label19.TabIndex = 53;
            this.label19.Text = "GraphType";
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(944, 531);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.cmbGraphType);
            this.Controls.Add(this.flp);
            this.Controls.Add(this.btTeX);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.nudYAxisTitleFontSize);
            this.Controls.Add(this.tbYAxisTitle);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.nudXAxisTitleFontSize);
            this.Controls.Add(this.tbXAxisTitle);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.nudTitleFontSize);
            this.Controls.Add(this.tbTitleText);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.nudYNotchedFontSize);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.nudXNotchedFontSize);
            this.Controls.Add(this.cmbSubAxisY);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cmbSubAxisX);
            this.Controls.Add(this.cmbMainAxisY);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cmbMainAxisX);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.nudYDigit);
            this.Controls.Add(this.nudXDigit);
            this.Controls.Add(this.nudYAxisMin);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.nudXAxisMin);
            this.Controls.Add(this.nudYAxisMax);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.nudXAxisMax);
            this.Controls.Add(this.nudYSubThickness);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nudXSubThickness);
            this.Controls.Add(this.nudYDistance);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nudXDistance);
            this.Controls.Add(this.nudYAxisThickness);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nudXAxisThickness);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nudDiagonalY);
            this.Controls.Add(this.nudDiagonalX);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudOriginY);
            this.Controls.Add(this.nudOriginX);
            this.Controls.Add(this.nudHeight);
            this.Controls.Add(this.nudWidth);
            this.Controls.Add(this.pb);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "MainForm ver.2021.11.20";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            ((System.ComponentModel.ISupportInitialize)(this.pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOriginY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudOriginX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiagonalY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiagonalX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXDistance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYSubThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXSubThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYDigit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXDigit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYNotchedFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXNotchedFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTitleFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudXAxisTitleFontSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudYAxisTitleFontSize)).EndInit();
            this.flp.ResumeLayout(false);
            this.flp.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pb;
        private System.Windows.Forms.NumericUpDown nudWidth;
        private System.Windows.Forms.NumericUpDown nudHeight;
        private System.Windows.Forms.NumericUpDown nudOriginY;
        private System.Windows.Forms.NumericUpDown nudOriginX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudDiagonalY;
        private System.Windows.Forms.NumericUpDown nudDiagonalX;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudXAxisThickness;
        private System.Windows.Forms.NumericUpDown nudYAxisThickness;
        private System.Windows.Forms.NumericUpDown nudYDistance;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown nudXDistance;
        private System.Windows.Forms.NumericUpDown nudYSubThickness;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudXSubThickness;
        private System.Windows.Forms.NumericUpDown nudYAxisMax;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown nudXAxisMax;
        private System.Windows.Forms.NumericUpDown nudYAxisMin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudXAxisMin;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudYDigit;
        private System.Windows.Forms.NumericUpDown nudXDigit;
        private System.Windows.Forms.ComboBox cmbMainAxisX;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbMainAxisY;
        private System.Windows.Forms.ComboBox cmbSubAxisY;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cmbSubAxisX;
        private System.Windows.Forms.NumericUpDown nudYNotchedFontSize;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudXNotchedFontSize;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tbTitleText;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudTitleFontSize;
        private System.Windows.Forms.TextBox tbXAxisTitle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudXAxisTitleFontSize;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown nudYAxisTitleFontSize;
        private System.Windows.Forms.TextBox tbYAxisTitle;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.FlowLayoutPanel flp;
        private System.Windows.Forms.CheckBox cbLegendVisible;
        private System.Windows.Forms.Button btTeX;
        private System.Windows.Forms.SaveFileDialog sfd;
        private System.Windows.Forms.ComboBox cmbGraphType;
        private System.Windows.Forms.Label label19;
    }
}

