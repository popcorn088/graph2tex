﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graph2TeX
{
    public partial class LineControl : UserControl
    {
        public LineControl(LineData lineData)
        {
            InitializeComponent();
            cmbLineType.DataSource = Graph2TeXAPI.lineTypes.Clone();
            cmbLineType.DataBindings.Add("SelectedItem", lineData, "lineType", true, DataSourceUpdateMode.OnPropertyChanged);
            nudThickness.DataBindings.Add("Value", lineData, "Thickness", true, DataSourceUpdateMode.OnPropertyChanged);
            tbTitle.DataBindings.Add("Text", lineData.Title, "text", true, DataSourceUpdateMode.OnPropertyChanged);
            nudFontSize.DataBindings.Add("Value", lineData.Title, "size_pt", true, DataSourceUpdateMode.OnPropertyChanged);
        }
    }
}
