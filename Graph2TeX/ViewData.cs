﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.IO;

namespace Graph2TeX
{
    class ViewData
    {
        public event EventHandler PropertyChanged;
        public event EventHandler LineDataAdded;

        public ViewData()
        {
            LegendVisible = true;
            Title = new Text("Graph Title", 12.0F);
            Title.PropertyChanged += Title_PropertyChanged;
            graphData = new GraphData();
            graphData.DataAdded += GraphData_DataAdded;
            XAxis = new MainAxisData();
            XAxis.Title.text = "x";
            XAxis.PropertyChanged += XAxis_PropertyChanged;
            XAxis.Title.PropertyChanged += Title_PropertyChanged1;
            YAxis = new MainAxisData();
            YAxis.Title.text = "y";
            YAxis.PropertyChanged += YAxis_PropertyChanged;
            YAxis.Title.PropertyChanged += Title_PropertyChanged2;

        }

        private void GraphData_PropertyChanged(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        private void GraphData_DataAdded(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        private void Title_PropertyChanged2(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        private void Title_PropertyChanged1(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        private void Title_PropertyChanged(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        private void YAxis_PropertyChanged(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        private void XAxis_PropertyChanged(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        protected GraphType _graphType;
        public GraphType graphType
        {
            get
            {
                return _graphType;
            }
            set
            {
                if (graphType == value)
                {
                    return;
                }

                _graphType = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        public Text Title;

        protected SizeF DrawSize;
        public float Width
        {
            get
            {
                return DrawSize.Width;
            }
            set
            {
                if (DrawSize.Width == value)
                {
                    return;
                }

                DrawSize.Width = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }
        public float Height
        {
            get
            {
                return DrawSize.Height;
            }
            set
            {
                if (DrawSize.Height == value)
                {
                    return;
                }

                DrawSize.Height = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        protected PointF Origin;
        public float OriginX
        {
            get
            {
                return Origin.X;
            }
            set
            {
                if (Origin.X == value)
                {
                    return;
                }

                Origin.X = value;
                if(PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }
        public float OriginY
        {
            get
            {
                return Origin.Y;
            }
            set
            {
                if (Origin.Y == value)
                {
                    return;
                }

                Origin.Y = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        public static float mmTopix(float Resolution, float mm)
        {
            return mm * Resolution / 25.4F;
        }

        public static PointF mmTopix(PointF Resolution, PointF mm)
        {
            return new PointF(mmTopix(Resolution.X, mm.X), mmTopix(Resolution.Y, mm.Y));
        }

        public static SizeF mmTopix(PointF Resolution, SizeF mm)
        {
            return new SizeF(mmTopix(Resolution.X, mm.Width), mmTopix(Resolution.Y, mm.Height));
        }

        public static float ptTopix(float Resolution, float pt)
        {
            return Resolution / 72.0F * pt;
        }

        public static PointF pixTodisplay(SizeF size, PointF point)
        {
            return new PointF(point.X, size.Height - point.Y);
        }

        public static float mmToinch(float mm)
        {
            return mm / 25.4F;
        }

        public static PointF mmToinch(PointF mm)
        {
            return new PointF(mmToinch(mm.X), mmToinch(mm.Y));
        }
        public static SizeF mmToinch(SizeF mm)
        {
            return new SizeF(mmToinch(mm.Width), mmToinch(mm.Height));
        }

        public static float ptTominch(float pt)
        {
            return 1000.0F / 72.0F * pt;
        }

        public static float ptTomm(float pt)
        {
            return 25.4F / 70.0F * pt;
        }

        public float DiagonalX
        {
            get
            {
                return Diagonal.X;
            }
            set
            {
                if (Diagonal.X == value)
                {
                    return;
                }

                Diagonal.X = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }
        public float DiagonalY
        {
            get
            {
                return Diagonal.Y;
            }
            set
            {
                if (Diagonal.Y == value)
                {
                    return;
                }

                Diagonal.Y = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }
        protected PointF Diagonal;

        public SizeF GraphSize
        {
            get
            {
                return new SizeF(Diagonal.X - Origin.X, Diagonal.Y - Origin.X);
            }
        }

        public MainAxisData XAxis { get; set; }
        public MainAxisData YAxis { get; set; }

        public GraphData graphData { get; set; }

        bool _LegendVisible;
        public bool LegendVisible
        {
            get
            {
                return _LegendVisible;
            }
            set
            {
                if (_LegendVisible == value)
                {
                    return;
                }

                _LegendVisible = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        public void LoadFile(string infile)
        {
            // ファイルからの読み込み
            StreamReader sr;
            sr = new StreamReader(infile);
            List<string[]> col_str_list = new List<string[]>();
            // コメントアウト行，デリミタ(,)で区切った結果をcol_str_listに格納してファイルは閉じる
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();

                // 先頭に#はコメントアウト行
                if (line.StartsWith("#") == true)
                {
                    continue;
                }

                // カンマ区切り
                string[] cols = line.Split(new string[] { "," }, StringSplitOptions.None);
                col_str_list.Add(cols);
            }
            sr.Close();

            // ファイル内容の処理
            // 1列ごとに処理していく
            // 0列目はx値，n列目以降はy_n値
            for (int col = 1; col < col_str_list[0].Length; col++)
            {
                List<PointF> tmp = new List<PointF>();
                for (int row = 0; row < col_str_list.Count; row++)
                {
                    float x = float.Parse(col_str_list[row][0]);
                    float y = 0.0F;
                    if (float.TryParse(col_str_list[row][col], out y) == false)
                    {
                        continue;
                    }

                    PointF cell = new PointF(x, y);
                    tmp.Add(cell);
                }

                graphData.Add(tmp.ToArray(), new Text("", 8.0F));
            }

            XAxis.Auto(graphData.XMaxMin);    // x軸の自動設定
            YAxis.Auto(graphData.YMaxMin);    // y軸の自動設定

            if (LineDataAdded != null)
            {
                LineDataAdded(this, EventArgs.Empty);
            }

            for (int i = 0; i < graphData.lines.Count; i++)
            {
                graphData.lines[i].Title.PropertyChanged += ViewData_PropertyChanged;
            }
        }

        private void ViewData_PropertyChanged(object sender, EventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }
        }

        /// <summary>
        /// 文字列の描画、回転、基準位置指定
        /// </summary>
        /// <param name="g">描画先のGraphicsオブジェクト</param>
        /// <param name="s">描画する文字列</param>
        /// <param name="f">文字のフォント</param>
        /// <param name="brush">描画用ブラシ</param>
        /// <param name="x">基準位置のX座標</param>
        /// <param name="y">基準位置のY座標</param>
        /// <param name="deg">回転角度（度数、時計周りが正）</param>
        /// <param name="format">基準位置をStringFormatクラスオブジェクトで指定します</param>
        public static void DrawString(Graphics g, string s, Font f, SolidBrush brush, float x, float y, float deg, StringFormat format)
        {
            using (var pathText = new System.Drawing.Drawing2D.GraphicsPath())  // パスの作成
            using (var mat = new System.Drawing.Drawing2D.Matrix())             // アフィン変換行列
            {
                // 描画用Format
                var formatTemp = (StringFormat)format.Clone();
                formatTemp.Alignment = StringAlignment.Near;        // 左寄せに修正
                formatTemp.LineAlignment = StringAlignment.Near;    // 上寄せに修正

                // 文字列の描画
                pathText.AddString(
                        s,
                        f.FontFamily,
                        (int)f.Style,
                        f.SizeInPoints,
                        new PointF(0, 0),
                        format);
                formatTemp.Dispose();

                // 文字の領域取得
                var rect = pathText.GetBounds();

                // 回転中心のX座標
                float px;
                switch (format.Alignment)
                {
                    case StringAlignment.Near:
                        px = rect.Left;
                        break;
                    case StringAlignment.Center:
                        px = rect.Left + rect.Width / 2f;
                        break;
                    case StringAlignment.Far:
                        px = rect.Right;
                        break;
                    default:
                        px = 0;
                        break;
                }
                // 回転中心のY座標
                float py;
                switch (format.LineAlignment)
                {
                    case StringAlignment.Near:
                        py = rect.Top;
                        break;
                    case StringAlignment.Center:
                        py = rect.Top + rect.Height / 2f;
                        break;
                    case StringAlignment.Far:
                        py = rect.Bottom;
                        break;
                    default:
                        py = 0;
                        break;
                }

                // 文字の回転中心座標を原点へ移動
                mat.Translate(-px, -py, System.Drawing.Drawing2D.MatrixOrder.Append);
                // 文字の回転
                mat.Rotate(deg, System.Drawing.Drawing2D.MatrixOrder.Append);
                // 表示位置まで移動
                mat.Translate(x, y, System.Drawing.Drawing2D.MatrixOrder.Append);

                // パスをアフィン変換
                pathText.Transform(mat);

                // 描画
                g.FillPath(brush, pathText);
            }
        }
    }
}
