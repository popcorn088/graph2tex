﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;

namespace Graph2TeX
{
    public class GraphData
    {
        //public event EventHandler PropertyChanged;
        public event EventHandler DataAdded;
        public List<LineData> lines { get; set; }
        public MaxMin XMaxMin;
        public MaxMin YMaxMin;

        public GraphData()
        {
            lines = new List<LineData>();
        }

        public void Add(PointF[] points, Text title)
        {
            LineData data = new LineData(points);
            data.PropertyChanged += Data_PropertyChanged;
            //data.Title.PropertyChanged += Title_PropertyChanged;

            // 生データのコピー
            data.RawData = new PointF[points.Length];
            for (int i = 0; i < points.Length; i++)
            {
                data.RawData[i] = points[i];    // コピー
                // x,y軸の最大値と最小値を更新
                XMaxMin.Max = Math.Max(XMaxMin.Max, data.RawData[i].X);
                XMaxMin.Min = Math.Min(XMaxMin.Min, data.RawData[i].X);
                YMaxMin.Max = Math.Max(YMaxMin.Max, data.RawData[i].Y);
                YMaxMin.Min = Math.Min(YMaxMin.Min, data.RawData[i].Y);
            }

            // タイトルのコピー
            data.Title = new Text(title);

            // 線種の決定
            // 他の線種となるべく被らないように線種を変える
            if (lines.Count != 0)
            {
                if (lines[lines.Count - 1].lineType == LineType.Solid)
                {
                    data.lineType = LineType.Dash;
                }
                else if (lines[lines.Count - 1].lineType == LineType.Dash)
                {
                    data.lineType = LineType.Dot;
                }
                else if (lines[lines.Count - 1].lineType == LineType.Dot)
                {
                    data.lineType = LineType.Solid;
                }
            }

            lines.Add(data);    // 追加完了
        }

        private void Title_PropertyChanged(object sender, EventArgs e)
        {
            /*
            if (PropertyChanged != null)
            {
                PropertyChanged(sender, e);
            }*/
        }

        private void Data_PropertyChanged(object sender, EventArgs e)
        {
            if (DataAdded != null)
            {
                DataAdded(sender, e);
            }
        }
    }
}
