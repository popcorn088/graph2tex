﻿
namespace Graph2TeX
{
    partial class LineControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbLineType = new System.Windows.Forms.ComboBox();
            this.nudThickness = new System.Windows.Forms.NumericUpDown();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.nudFontSize = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nudThickness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSize)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbLineType
            // 
            this.cmbLineType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLineType.FormattingEnabled = true;
            this.cmbLineType.Location = new System.Drawing.Point(3, 3);
            this.cmbLineType.Name = "cmbLineType";
            this.cmbLineType.Size = new System.Drawing.Size(121, 20);
            this.cmbLineType.TabIndex = 0;
            // 
            // nudThickness
            // 
            this.nudThickness.Location = new System.Drawing.Point(3, 29);
            this.nudThickness.Name = "nudThickness";
            this.nudThickness.Size = new System.Drawing.Size(120, 19);
            this.nudThickness.TabIndex = 1;
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(3, 54);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(120, 19);
            this.tbTitle.TabIndex = 2;
            // 
            // nudFontSize
            // 
            this.nudFontSize.Location = new System.Drawing.Point(3, 79);
            this.nudFontSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudFontSize.Name = "nudFontSize";
            this.nudFontSize.Size = new System.Drawing.Size(120, 19);
            this.nudFontSize.TabIndex = 3;
            this.nudFontSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LineControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nudFontSize);
            this.Controls.Add(this.tbTitle);
            this.Controls.Add(this.nudThickness);
            this.Controls.Add(this.cmbLineType);
            this.Name = "LineControl";
            this.Size = new System.Drawing.Size(136, 109);
            ((System.ComponentModel.ISupportInitialize)(this.nudThickness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFontSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbLineType;
        private System.Windows.Forms.NumericUpDown nudThickness;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.NumericUpDown nudFontSize;
    }
}
