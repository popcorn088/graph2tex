﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graph2TeX
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] filename = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            
            graph2tex.LoadFile(filename[0]);
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        /*
        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bmp = new Bitmap(pb.Width, pb.Height);
            Image img = (Image)bmp;

            Graph2TeXAPI graph2tex = new Graph2TeXAPI(img);
            graph2tex.LoadFile("sample2.csv");
            graph2tex.Width = 130;
            graph2tex.Height = 130;
            graph2tex.XAxis.Thickness = 2.0F;
            graph2tex.YAxis.Thickness = 2.0F;
            graph2tex.OriginX = 10;
            graph2tex.OriginY = 10;
            graph2tex.DiagonalX = 100;
            graph2tex.DiagonalY = 100;

            graph2tex.Draw(img);

            pb.Image = img;
        }*/

        Bitmap bmp;
        Image img;
        Graph2TeXAPI graph2tex;
        private void MainForm_Load(object sender, EventArgs e)
        {
            graph2tex = new Graph2TeXAPI();
            graph2tex.LineDataAdded += Graph2tex_LineDataAdded;
            //graph2tex.LoadFile("sample2.csv");
            graph2tex.Width = 100;
            graph2tex.Height = 100;
            graph2tex.XAxis.Thickness = 2.0F;
            graph2tex.YAxis.Thickness = 2.0F;
            graph2tex.OriginX = 10;
            graph2tex.OriginY = 10;
            graph2tex.DiagonalX = 100;
            graph2tex.DiagonalY = 100;

            cmbGraphType.DataSource = Graph2TeXAPI.graphTypes.Clone();
            cmbMainAxisX.DataSource = Graph2TeXAPI.lineTypes.Clone();
            cmbMainAxisY.DataSource = Graph2TeXAPI.lineTypes.Clone();
            cmbSubAxisX.DataSource = Graph2TeXAPI.lineTypes.Clone();
            cmbSubAxisY.DataSource = Graph2TeXAPI.lineTypes.Clone();

            cmbGraphType.DataBindings.Add("SelectedItem", graph2tex, "graphType", true, DataSourceUpdateMode.OnPropertyChanged);
            tbTitleText.DataBindings.Add("Text", graph2tex.Title, "text", true, DataSourceUpdateMode.OnPropertyChanged);
            nudTitleFontSize.DataBindings.Add("Value", graph2tex.Title, "size_pt", true, DataSourceUpdateMode.OnPropertyChanged);
            nudWidth.DataBindings.Add("Value", graph2tex, "Width", true, DataSourceUpdateMode.OnPropertyChanged);
            nudHeight.DataBindings.Add("Value", graph2tex, "Height", true, DataSourceUpdateMode.OnPropertyChanged);
            nudOriginX.DataBindings.Add("Value", graph2tex, "OriginX", true, DataSourceUpdateMode.OnPropertyChanged);
            nudOriginY.DataBindings.Add("Value", graph2tex, "OriginY", true, DataSourceUpdateMode.OnPropertyChanged);
            nudDiagonalX.DataBindings.Add("Value", graph2tex, "DiagonalX", true, DataSourceUpdateMode.OnPropertyChanged);
            nudDiagonalY.DataBindings.Add("Value", graph2tex, "DiagonalY", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXAxisThickness.DataBindings.Add("Value", graph2tex.XAxis, "Thickness", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYAxisThickness.DataBindings.Add("Value", graph2tex.YAxis, "Thickness", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXDistance.DataBindings.Add("Value", graph2tex.XAxis, "Distance", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYDistance.DataBindings.Add("Value", graph2tex.YAxis, "Distance", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXAxisMax.DataBindings.Add("Value", graph2tex.XAxis, "Max", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYAxisMax.DataBindings.Add("Value", graph2tex.YAxis, "Max", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXAxisMin.DataBindings.Add("Value", graph2tex.XAxis, "Min", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYAxisMin.DataBindings.Add("Value", graph2tex.YAxis, "Min", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXSubThickness.DataBindings.Add("Value", graph2tex.XAxis, "SubThickness", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYSubThickness.DataBindings.Add("Value", graph2tex.YAxis, "SubThickness", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXDigit.DataBindings.Add("Value", graph2tex.XAxis, "Digit", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYDigit.DataBindings.Add("Value", graph2tex.YAxis, "Digit", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXNotchedFontSize.DataBindings.Add("Value", graph2tex.XAxis, "NotchedFontSize", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYNotchedFontSize.DataBindings.Add("Value", graph2tex.YAxis, "NotchedFontSize", true, DataSourceUpdateMode.OnPropertyChanged);
            cmbMainAxisX.DataBindings.Add("SelectedItem", graph2tex.XAxis, "MainLineType", true, DataSourceUpdateMode.OnPropertyChanged);
            cmbMainAxisY.DataBindings.Add("SelectedItem", graph2tex.YAxis, "MainLineType", true, DataSourceUpdateMode.OnPropertyChanged);
            cmbSubAxisX.DataBindings.Add("SelectedItem", graph2tex.XAxis, "SubLineType", true, DataSourceUpdateMode.OnPropertyChanged);
            cmbSubAxisY.DataBindings.Add("SelectedItem", graph2tex.YAxis, "SubLineType", true, DataSourceUpdateMode.OnPropertyChanged);
            tbXAxisTitle.DataBindings.Add("Text", graph2tex.XAxis.Title, "text", true, DataSourceUpdateMode.OnPropertyChanged);
            nudXAxisTitleFontSize.DataBindings.Add("Value", graph2tex.XAxis.Title, "size_pt", true, DataSourceUpdateMode.OnPropertyChanged);
            tbYAxisTitle.DataBindings.Add("Text", graph2tex.YAxis.Title, "text", true, DataSourceUpdateMode.OnPropertyChanged);
            nudYAxisTitleFontSize.DataBindings.Add("Value", graph2tex.YAxis.Title, "size_pt", true, DataSourceUpdateMode.OnPropertyChanged);
            cbLegendVisible.DataBindings.Add("Checked", graph2tex, "LegendVisible", true, DataSourceUpdateMode.OnPropertyChanged);

            graph2tex.PropertyChanged += Graph2tex_PropertyChanged;
        }

        private void Graph2tex_LineDataAdded(object sender, EventArgs e)
        {
            for (int i = 0; i < graph2tex.graphData.lines.Count; i++)
            {
                LineControl lineControl = new LineControl(graph2tex.graphData.lines[i]);
                flp.Controls.Add(lineControl);
            }
        }

        private void Graph2tex_PropertyChanged(object sender, EventArgs e)
        {
            bmp = new Bitmap(pb.Width, pb.Height);
            img = (Image)bmp;

            graph2tex.Draw(img);

            pb.Image = img;
        }

        private void btTeX_Click(object sender, EventArgs e)
        {
            if (sfd.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            graph2tex.SaveTeX(sfd.FileName);
        }
    }
}
