﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graph2TeX
{
    public enum LineType
    {
        Solid,
        Dash,
        Dot
    }

    public enum GraphType
    {
        Liner,
        Scatter,
        Bar
    }

    public struct MaxMin
    {
        public float Min { get; set; }
        public float Max { get; set; }
    }

    public enum Alignment
    {
        Left,
        Center,
        Right
    }

    public enum LineAlignment
    {
        Top,
        Center,
        Bottom
    }
}
