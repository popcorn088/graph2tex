﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace Graph2TeX
{
    class Graph2TeXAPI : ViewData
    {
        public static GraphType[] graphTypes = new GraphType[] { GraphType.Liner, GraphType.Scatter, GraphType.Bar };
        public static LineType[] lineTypes = new LineType[] { LineType.Solid, LineType.Dash, LineType.Dot };
        public Graph2TeXAPI()
        {
        }

        public void Draw(Image _image)
        {
            Image image = _image;

            Graphics g = Graphics.FromImage(image);
            DrawFrame(g, new PointF(image.HorizontalResolution, image.VerticalResolution));
            DrawAxes(g, new PointF(image.HorizontalResolution, image.VerticalResolution));
            DrawSubAxes(g, new PointF(image.HorizontalResolution, image.VerticalResolution));
            switch (graphType)
            {
                case GraphType.Bar:
                    DrawGraphBar(g, new PointF(image.HorizontalResolution, image.VerticalResolution));
                    break;
                default:
                case GraphType.Liner:
                    DrawGraphLiner(g, new PointF(image.HorizontalResolution, image.VerticalResolution));
                    break;
            }
            DrawTitle(g, new PointF(image.HorizontalResolution, image.VerticalResolution));
            if (LegendVisible == true)
            {
                DrawLegend(g, new PointF(image.HorizontalResolution, image.VerticalResolution));
            }
        }

        private void DrawFrame(Graphics g, PointF Resolution)
        {
            SizeF DrawSizeDisp = mmTopix(Resolution, DrawSize);
            Pen pen = new Pen(Brushes.Red, 1.0F);
            g.DrawRectangle(pen, 0, 0, DrawSizeDisp.Width, DrawSizeDisp.Height);
        }

        private void DrawAxes(Graphics g, PointF Resolution)
        {
            SizeF DrawSizeDisp = mmTopix(Resolution, DrawSize);
            PointF OriginDisp = mmTopix(Resolution, Origin);    // 原点をpixに
            OriginDisp = pixTodisplay(DrawSizeDisp, OriginDisp);    // 上下反転

            PointF beg_pos = new PointF(mmTopix(Resolution.X, Origin.X), mmTopix(Resolution.Y, Origin.Y));
            PointF end_pos = new PointF(mmTopix(Resolution.X, Diagonal.X), beg_pos.Y);
            beg_pos = pixTodisplay(DrawSizeDisp, beg_pos);
            end_pos = pixTodisplay(DrawSizeDisp, end_pos);
            float thickness = ptTopix(Resolution.X, XAxis.Thickness);
            Pen pen = new Pen(Brushes.Black, thickness);
            switch (XAxis.MainLineType)
            {
                case LineType.Dash:
                    pen.DashStyle = DashStyle.Dash;
                    break;
                case LineType.Dot:
                    pen.DashStyle = DashStyle.Dot;
                    break;
                case LineType.Solid:
                default:
                    pen.DashStyle = DashStyle.Solid;
                    break;
            }
            g.DrawLine(pen, beg_pos, end_pos);

            string to_string_fmt = "F" + XAxis.Digit.ToString();
            string tick_str = XAxis.Min.ToString(to_string_fmt);
            StringFormat fmt = new StringFormat();
            fmt.Alignment = StringAlignment.Center;
            fmt.LineAlignment = StringAlignment.Near;
            Font font = new Font(FontFamily.GenericSansSerif, XAxis.NotchedFontSize);
            g.DrawString(tick_str, font, Brushes.Black, beg_pos, fmt);

            beg_pos = new PointF(mmTopix(Resolution.Y, Origin.X), mmTopix(Resolution.Y, Origin.Y));
            end_pos = new PointF(beg_pos.X, mmTopix(Resolution.Y, Diagonal.Y));
            beg_pos = pixTodisplay(DrawSizeDisp, beg_pos);
            end_pos = pixTodisplay(DrawSizeDisp, end_pos);
            thickness = ptTopix(Resolution.Y, YAxis.Thickness);
            pen = new Pen(Brushes.Black, thickness);
            switch (YAxis.MainLineType)
            {
                case LineType.Dash:
                    pen.DashStyle = DashStyle.Dash;
                    break;
                case LineType.Dot:
                    pen.DashStyle = DashStyle.Dot;
                    break;
                case LineType.Solid:
                default:
                    pen.DashStyle = DashStyle.Solid;
                    break;
            }
            g.DrawLine(pen, beg_pos, end_pos);

            to_string_fmt = "F" + YAxis.Digit.ToString();
            tick_str = YAxis.Min.ToString(to_string_fmt);
            fmt = new StringFormat();
            fmt.Alignment = StringAlignment.Far;
            fmt.LineAlignment = StringAlignment.Center;
            font = new Font(FontFamily.GenericSansSerif, YAxis.NotchedFontSize);
            g.DrawString(tick_str, font, Brushes.Black, beg_pos, fmt);
        }

        private string TeXAxes()
        {
            string ret = "";

            PointF beg_pos = Origin;
            PointF end_pos = new PointF(Diagonal.X, beg_pos.Y);
            ret += TeXLib.Line(XAxis.MainLineType, XAxis.Thickness, beg_pos, end_pos);
            string to_string_fmt = "F" + XAxis.Digit.ToString();
            string tick_str = XAxis.Min.ToString(to_string_fmt);
            ret += TeXLib.Text(tick_str, XAxis.NotchedFontSize, Alignment.Center, LineAlignment.Top, beg_pos);

            beg_pos = Origin;
            end_pos = new PointF(beg_pos.X, Diagonal.Y);
            ret += TeXLib.Line(YAxis.MainLineType, YAxis.Thickness, beg_pos, end_pos);
            to_string_fmt = "F" + YAxis.Digit.ToString();
            tick_str = YAxis.Min.ToString(to_string_fmt);
            ret += TeXLib.Text(tick_str, YAxis.NotchedFontSize, Alignment.Right, LineAlignment.Center, beg_pos);

            return ret;
        }

        private void DrawSubAxes(Graphics g, PointF Resolution)
        {
            SizeF DrawSizeDisp = mmTopix(Resolution, DrawSize);
            PointF OriginDisp = mmTopix(Resolution, Origin);    // 原点をpixに
            PointF DiagonalDisp = mmTopix(Resolution, Diagonal);    // 対角点をpixに
            OriginDisp = pixTodisplay(DrawSizeDisp, OriginDisp);    // 上下反転
            DiagonalDisp = pixTodisplay(DrawSizeDisp, DiagonalDisp);    // 上下反転

            float Ntmp = (XAxis.Max - XAxis.Min) / XAxis.Distance;
            int N = (int)Ntmp;
            //int N = (int)Math.Floor((XAxis.Max - XAxis.Min) / XAxis.Distance);
            SizeF GraphSizePix = new SizeF(DiagonalDisp.X - OriginDisp.X, OriginDisp.Y - DiagonalDisp.Y); // グラフ領域の幅pix
            PointF alpha = new PointF(GraphSizePix.Width / (XAxis.Max - XAxis.Min), GraphSizePix.Height / (YAxis.Max - YAxis.Min)); // graph2pix変換係数
            PointF Delta = new PointF(alpha.X * XAxis.Distance, alpha.Y * YAxis.Distance);
            string to_string_fmt = "F" + XAxis.Digit.ToString();
            float thickness = ptTopix(Resolution.X, XAxis.SubThickness);
            Pen pen = new Pen(Brushes.Black, thickness);
            switch (XAxis.SubLineType)
            {
                case LineType.Dash:
                    pen.DashStyle = DashStyle.Dash;
                    break;
                case LineType.Dot:
                    pen.DashStyle = DashStyle.Dot;
                    break;
                case LineType.Solid:
                default:
                    pen.DashStyle = DashStyle.Solid;
                    break;
            }
            for (int n = 1; n <= N; n++)
            {
                PointF beg_pos = new PointF(Delta.X * n + OriginDisp.X, OriginDisp.Y);
                PointF end_pos = new PointF(beg_pos.X, DiagonalDisp.Y);
                g.DrawLine(pen, beg_pos, end_pos);

                string tick_str = (XAxis.Min + XAxis.Distance * n).ToString(to_string_fmt);
                StringFormat fmt = new StringFormat();
                fmt.Alignment = StringAlignment.Center;
                fmt.LineAlignment = StringAlignment.Near;
                Font font = new Font(FontFamily.GenericSansSerif, XAxis.NotchedFontSize);
                g.DrawString(tick_str, font, Brushes.Black, beg_pos, fmt);
            }

            Ntmp = (YAxis.Max - YAxis.Min) / YAxis.Distance;
            N = (int)Ntmp;
            to_string_fmt = "F" + YAxis.Digit.ToString();
            thickness = ptTopix(Resolution.X, YAxis.SubThickness);
            pen = new Pen(Brushes.Black, thickness);
            switch (YAxis.SubLineType)
            {
                case LineType.Dash:
                    pen.DashStyle = DashStyle.Dash;
                    break;
                case LineType.Dot:
                    pen.DashStyle = DashStyle.Dot;
                    break;
                case LineType.Solid:
                default:
                    pen.DashStyle = DashStyle.Solid;
                    break;
            }
            for (int n = 1; n <= N; n++)
            {
                PointF beg_pos = new PointF(OriginDisp.X, OriginDisp.Y - Delta.Y * n);
                PointF end_pos = new PointF(DiagonalDisp.X, beg_pos.Y);
                g.DrawLine(pen, beg_pos, end_pos);

                string tick_str = (YAxis.Min + YAxis.Distance * n).ToString(to_string_fmt);
                StringFormat fmt = new StringFormat();
                fmt.Alignment = StringAlignment.Far;
                fmt.LineAlignment = StringAlignment.Center;
                Font font = new Font(FontFamily.GenericSansSerif, YAxis.NotchedFontSize);
                g.DrawString(tick_str, font, Brushes.Black, beg_pos,fmt);
            }
        }

        private string TeXSubAxes()
        {
            string ret = "";

            PointF alpha = new PointF(
                (Diagonal.X - Origin.Y) / (XAxis.Max - XAxis.Min),
                (Diagonal.Y - Origin.Y) / (YAxis.Max - YAxis.Min));
            PointF Delta = new PointF(alpha.X * XAxis.Distance, alpha.Y * YAxis.Distance);

            float Ntmp = (XAxis.Max - XAxis.Min) / XAxis.Distance;
            int N = (int)Ntmp;
            for (int n = 1; n <= N; n++)
            {
                PointF beg_pos = new PointF(Delta.X * n + Origin.X, Origin.Y);
                PointF end_pos = new PointF(beg_pos.X, Diagonal.Y);
                ret += TeXLib.Line(XAxis.SubLineType, XAxis.SubThickness, beg_pos, end_pos);
                string to_string_fmt = "F" + XAxis.Digit.ToString();
                string tick_str = (XAxis.Min + XAxis.Distance * n).ToString(to_string_fmt);
                ret += TeXLib.Text(tick_str, XAxis.NotchedFontSize, Alignment.Center, LineAlignment.Top, beg_pos);
            }

            Ntmp = (YAxis.Max - YAxis.Min) / YAxis.Distance;
            N = (int)Ntmp;
            for (int n = 1; n <= N; n++)
            {
                PointF beg_pos = new PointF(Origin.X, Origin.Y + Delta.Y * n);
                PointF end_pos = new PointF(Diagonal.X, beg_pos.Y);
                ret += TeXLib.Line(YAxis.SubLineType, YAxis.SubThickness, beg_pos, end_pos);
                string to_string_fmt = "F" + YAxis.Digit.ToString();
                string tick_str = (YAxis.Min + YAxis.Distance * n).ToString(to_string_fmt);
                ret += TeXLib.Text(tick_str, YAxis.NotchedFontSize, Alignment.Right, LineAlignment.Center, beg_pos);
            }


            return ret;
        }
        private void DrawGraphLiner(Graphics g, PointF Resolution)
        {
            SizeF DrawSizeDisp = mmTopix(Resolution, DrawSize);
            PointF OriginDisp = mmTopix(Resolution, Origin);    // 原点をpixに
            PointF DiagonalDisp = mmTopix(Resolution, Diagonal);    // 対角点をpixに
            OriginDisp = pixTodisplay(DrawSizeDisp, OriginDisp);    // 上下反転
            DiagonalDisp = pixTodisplay(DrawSizeDisp, DiagonalDisp);    // 上下反転
            SizeF GraphSizePix = new SizeF(DiagonalDisp.X - OriginDisp.X, OriginDisp.Y - DiagonalDisp.Y); // グラフ領域の幅pix
            float alpha_x = GraphSizePix.Width / (XAxis.Max - XAxis.Min);
            PointF alpha = new PointF(GraphSizePix.Width / (XAxis.Max - XAxis.Min), GraphSizePix.Height / (YAxis.Max - YAxis.Min)); // graph2pix変換係数

            for (int i = 0; i < graphData.lines.Count; i++)
            {
                float thickness = ptTopix(Resolution.X, graphData.lines[i].Thickness);
                Pen pen = new Pen(Brushes.Black, thickness);
                switch (graphData.lines[i].lineType)
                {
                    case LineType.Dash:
                        pen.DashStyle = DashStyle.Dash;
                        break;
                    case LineType.Dot:
                        pen.DashStyle = DashStyle.Dot;
                        break;
                    case LineType.Solid:
                    default:
                        pen.DashStyle = DashStyle.Solid;
                        break;
                }
                for (int j = 0; j < graphData.lines[i].RawData.Length - 1; j++)
                {
                    PointF beg_data = graphData.lines[i].RawData[j];
                    PointF beg_pos = new PointF();
                    beg_pos.X = (beg_data.X - XAxis.Min) * alpha.X + OriginDisp.X;
                    beg_pos.Y = OriginDisp.Y - (beg_data.Y - YAxis.Min) * alpha.Y;

                    PointF end_data = graphData.lines[i].RawData[j + 1];
                    PointF end_pos = new PointF();
                    end_pos.X = (end_data.X - XAxis.Min) * alpha.X + OriginDisp.X;
                    end_pos.Y = OriginDisp.Y - (end_data.Y - YAxis.Min) * alpha.Y;

                    if(!((XAxis.Min <= beg_data.X) && (beg_data.X <= XAxis.Max)))
                    {
                        continue;
                    }
                    if (!((XAxis.Min <= end_data.X) && (end_data.X <= XAxis.Max)))
                    {
                        continue;
                    }

                    if (!((YAxis.Min <= beg_data.Y) && (beg_data.Y <= YAxis.Max)))
                    {
                        continue;
                    }
                    if (!((YAxis.Min <= end_data.Y) && (end_data.Y <= YAxis.Max)))
                    {
                        continue;
                    }

                    g.DrawLine(pen, beg_pos, end_pos);
                }
            }
        }

        private string TeXGraphLiner()
        {
            string ret = "";

            PointF alpha = new PointF(
                (Diagonal.X - Origin.Y) / (XAxis.Max - XAxis.Min),
                (Diagonal.Y - Origin.Y) / (YAxis.Max - YAxis.Min));
            PointF Delta = new PointF(alpha.X * XAxis.Distance, alpha.Y * YAxis.Distance);

            for (int i = 0; i < graphData.lines.Count; i++)
            {
                List<PointF> curve = new List<PointF>();
                for (int j = 0; j < graphData.lines[i].RawData.Length; j++)
                {
                    PointF beg_data = graphData.lines[i].RawData[j];
                    PointF beg_pos = new PointF();
                    beg_pos.X = (beg_data.X - XAxis.Min) * alpha.X + Origin.X;
                    beg_pos.Y = (beg_data.Y - YAxis.Min) * alpha.Y + Origin.Y;

                    if (!((XAxis.Min <= beg_data.X) && (beg_data.X <= XAxis.Max)))
                    {
                        ret += TeXLib.Line(graphData.lines[i].lineType, graphData.lines[i].Thickness, curve.ToArray());
                        continue;
                    }

                    if (!((YAxis.Min <= beg_data.Y) && (beg_data.Y <= YAxis.Max)))
                    {
                        ret += TeXLib.Line(graphData.lines[i].lineType, graphData.lines[i].Thickness, curve.ToArray());
                        continue;
                    }

                    curve.Add(beg_pos);
                }
                ret += TeXLib.Line(graphData.lines[i].lineType, graphData.lines[i].Thickness, curve.ToArray());
            }

            return ret;
        }

        private void DrawGraphBar(Graphics g, PointF Resolution)
        {
            SizeF DrawSizeDisp = mmTopix(Resolution, DrawSize);
            PointF OriginDisp = mmTopix(Resolution, Origin);    // 原点をpixに
            PointF DiagonalDisp = mmTopix(Resolution, Diagonal);    // 対角点をpixに
            OriginDisp = pixTodisplay(DrawSizeDisp, OriginDisp);    // 上下反転
            DiagonalDisp = pixTodisplay(DrawSizeDisp, DiagonalDisp);    // 上下反転
            SizeF GraphSizePix = new SizeF(DiagonalDisp.X - OriginDisp.X, OriginDisp.Y - DiagonalDisp.Y); // グラフ領域の幅pix
            float alpha_x = GraphSizePix.Width / (XAxis.Max - XAxis.Min);
            PointF alpha = new PointF(GraphSizePix.Width / (XAxis.Max - XAxis.Min), GraphSizePix.Height / (YAxis.Max - YAxis.Min)); // graph2pix変換係数

            for (int i = 0; i < graphData.lines.Count; i++)
            {
                float thickness = ptTopix(Resolution.X, graphData.lines[i].Thickness);
                Pen pen = new Pen(Brushes.Black, thickness);
                switch (graphData.lines[i].lineType)
                {
                    case LineType.Dash:
                        pen.DashStyle = DashStyle.Dash;
                        break;
                    case LineType.Dot:
                        pen.DashStyle = DashStyle.Dot;
                        break;
                    case LineType.Solid:
                    default:
                        pen.DashStyle = DashStyle.Solid;
                        break;
                }
                for (int j = 0; j < graphData.lines[i].RawData.Length; j++)
                {
                    PointF beg_data = graphData.lines[i].RawData[j];
                    PointF beg_pos = new PointF();
                    beg_pos.X = (beg_data.X - XAxis.Min) * alpha.X + OriginDisp.X;
                    beg_pos.Y = OriginDisp.Y - (beg_data.Y - YAxis.Min) * alpha.Y;

                    PointF end_pos = new PointF(beg_pos.X, OriginDisp.Y);

                    if (!((XAxis.Min <= beg_data.X) && (beg_data.X <= XAxis.Max)))
                    {
                        continue;
                    }

                    if (!((YAxis.Min <= beg_data.Y) && (beg_data.Y <= YAxis.Max)))
                    {
                        continue;
                    }

                    g.DrawLine(pen, beg_pos, end_pos);
                }
            }
        }

        private string TeXGraphBar()
        {
            string ret = "";

            PointF alpha = new PointF(
                (Diagonal.X - Origin.Y) / (XAxis.Max - XAxis.Min),
                (Diagonal.Y - Origin.Y) / (YAxis.Max - YAxis.Min));
            PointF Delta = new PointF(alpha.X * XAxis.Distance, alpha.Y * YAxis.Distance);

            for (int i = 0; i < graphData.lines.Count; i++)
            {
                List<PointF> curve = new List<PointF>();
                for (int j = 0; j < graphData.lines[i].RawData.Length; j++)
                {
                    PointF beg_data = graphData.lines[i].RawData[j];
                    PointF beg_pos = new PointF();
                    beg_pos.X = (beg_data.X - XAxis.Min) * alpha.X + Origin.X;
                    beg_pos.Y = (beg_data.Y - YAxis.Min) * alpha.Y + Origin.Y;

                    PointF end_pos = new PointF(beg_pos.X, Origin.Y);

                    ret += TeXLib.Line(graphData.lines[i].lineType, graphData.lines[i].Thickness, beg_pos, end_pos);
                }
            }

            return ret;
        }

        private void DrawTitle(Graphics g, PointF Resolution)
        {
            SizeF DrawSizeDisp = mmTopix(Resolution, DrawSize);
            StringFormat fmt = new StringFormat();
            fmt.Alignment = StringAlignment.Center;
            fmt.LineAlignment = StringAlignment.Near;
            Font font = new Font(FontFamily.GenericSansSerif, Title.size_pt);
            g.DrawString(Title.text, font, Brushes.Black, new PointF(DrawSizeDisp.Width / 2.0F, 0.0F), fmt);

            PointF OriginDisp = mmTopix(Resolution, Origin);    // 原点をpixに
            PointF DiagonalDisp = mmTopix(Resolution, Diagonal);    // 対角点をpixに
            OriginDisp = pixTodisplay(DrawSizeDisp, OriginDisp);    // 上下反転
            DiagonalDisp = pixTodisplay(DrawSizeDisp, DiagonalDisp);    // 上下反転
            PointF TitlePoint = new PointF((DiagonalDisp.X + OriginDisp.X) / 2.0F, DrawSizeDisp.Height);
            fmt = new StringFormat();
            fmt.Alignment = StringAlignment.Center;
            fmt.LineAlignment = StringAlignment.Far;
            font = new Font(FontFamily.GenericSansSerif, XAxis.Title.size_pt);
            g.DrawString(XAxis.Title.text, font, Brushes.Black, TitlePoint, fmt);

            TitlePoint = new PointF(0, (DiagonalDisp.Y + OriginDisp.Y) / 2.0F);
            fmt = new StringFormat();
            fmt.Alignment = StringAlignment.Near;
            fmt.LineAlignment = StringAlignment.Center;
            font = new Font(FontFamily.GenericSansSerif, YAxis.Title.size_pt * 1.5F);
            //g.DrawString(YAxis.Title.text, font, Brushes.Black, TitlePoint, fmt);
            SolidBrush solb = new SolidBrush(Color.Black);
            ViewData.DrawString(g, YAxis.Title.text, font, solb, TitlePoint.X + 10, TitlePoint.Y, -90, fmt);
        }

        private string TeXTitle()
        {
            string ret = "";

            PointF beg_pos = new PointF(DrawSize.Width / 2.0F, DrawSize.Height);
            ret += TeXLib.Text(this.Title.text, this.Title.size_pt, Alignment.Center, LineAlignment.Top, beg_pos);

            beg_pos = new PointF((Origin.X + Diagonal.X) / 2.0F, 0.0F);
            ret += TeXLib.Text(XAxis.Title.text, XAxis.Title.size_pt, Alignment.Center, LineAlignment.Bottom, beg_pos);

            beg_pos = new PointF(0.0F, (Origin.Y + Diagonal.Y) / 2.0F);
            ret += TeXLib.Text(YAxis.Title.text, YAxis.Title.size_pt, Alignment.Center, LineAlignment.Bottom, 90, Alignment.Center, beg_pos);

            return ret;
        }

        private void DrawLegend(Graphics g, PointF Resolution)
        {
            SizeF DrawSizeDisp = mmTopix(Resolution, DrawSize);
            PointF DiagonalDisp = mmTopix(Resolution, Diagonal);    // 原点をpixに
            DiagonalDisp = pixTodisplay(DrawSizeDisp, DiagonalDisp);    // 上下反転

            float sum_font_height = 0;
            for (int i = 0; i < graphData.lines.Count; i++)
            {
                float thickness = ptTopix(Resolution.X, graphData.lines[i].Thickness);
                Pen pen = new Pen(Brushes.Black, thickness);
                switch (graphData.lines[i].lineType)
                {
                    case LineType.Dash:
                        pen.DashStyle = DashStyle.Dash;
                        break;
                    case LineType.Dot:
                        pen.DashStyle = DashStyle.Dot;
                        break;
                    case LineType.Solid:
                    default:
                        pen.DashStyle = DashStyle.Solid;
                        break;
                }

                float margin = mmTopix(Resolution.X, 3);
                sum_font_height += ptTopix(Resolution.Y, graphData.lines[i].Title.size_pt);
                PointF beg_pos = new PointF(DiagonalDisp.X + margin, DiagonalDisp.Y + sum_font_height);
                float length = mmTopix(Resolution.X, 7);
                PointF end_pos = new PointF(beg_pos.X + length, beg_pos.Y);
                g.DrawLine(pen, beg_pos, end_pos);

                Font font = new Font(FontFamily.GenericSansSerif, graphData.lines[i].Title.size_pt);
                StringFormat fmt = new StringFormat();
                fmt.Alignment = StringAlignment.Near;
                fmt.LineAlignment = StringAlignment.Center;
                fmt.FormatFlags = StringFormatFlags.NoWrap;
                g.DrawString(graphData.lines[i].Title.text, font, Brushes.Black, end_pos, fmt);
            }
        }

        private string TeXLegend()
        {
            string ret = "";
            float sum_font_height = 0;
            float margin = 3.0F;
            float length = 7.0F;
            for (int i = 0; i < graphData.lines.Count; i++)
            {
                sum_font_height += ptTomm(graphData.lines[i].Title.size_pt);
                PointF beg_pos = new PointF(Diagonal.X + margin, Diagonal.Y - sum_font_height);
                PointF end_pos = new PointF(beg_pos.X + length, beg_pos.Y);
                ret += TeXLib.Line(graphData.lines[i].lineType, graphData.lines[i].Thickness, beg_pos, end_pos);
                ret += TeXLib.Text(graphData.lines[i].Title.text, graphData.lines[i].Title.size_pt, Alignment.Left, LineAlignment.Center, end_pos);
            }

            return ret;
        }

        public void SaveTeX(string outfile)
        {
            string ret = TeX();

            StreamWriter sw = new StreamWriter(outfile);
            sw.Write(ret);
            sw.Close();
        }

        private string TeX()
        {
            string ret = TeXLib.BeginPicture(DrawSize);
            ret += TeXAxes();
            ret += TeXSubAxes();
            switch (graphType)
            {
                case GraphType.Bar:
                    ret += TeXGraphBar();
                    break;
                case GraphType.Liner:
                default:
                    ret += TeXGraphLiner();
                    break;
            }
            ret += TeXTitle();
            ret += TeXLegend();

            ret += TeXLib.EndPicture();

            return ret;
        }
    }
}
