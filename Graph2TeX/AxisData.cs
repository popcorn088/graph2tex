﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graph2TeX
{
    public class MainAxisData
    {
        public event EventHandler PropertyChanged;
        public Text Title;

        int _Digit;
        public int Digit 
        { 
            get
            {
                return _Digit;
            }
            set
            {
                if (_Digit == value)
                {
                    return;
                }

                _Digit = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        //public float Position { get; set; }

        float _Max;
        public float Max
        { 
            get
            {
                return _Max;
            }
            set
            {
                if (_Max == value)
                {
                    return;
                }

                _Max = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        float _Min;
        public float Min 
        { 
            get
            {
                return _Min;
            }
            set
            {
                if (_Min == value)
                {
                    return;
                }

                _Min = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        float _Thickness;
        public float Thickness
        { 
            get
            {
                return _Thickness;
            }
            set
            {
                if (_Thickness == value)
                {
                    return;
                }

                _Thickness = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        float _Distance;
        public float Distance
        {
            get
            {
                return _Distance;
            }
            set
            {
                if (_Distance == value)
                {
                    return;
                }

                _Distance = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        float _NotchedFontSize;
        public float NotchedFontSize
        {
            get
            {
                return _NotchedFontSize;
            }
            set
            {
                if (_NotchedFontSize == value)
                {
                    return;
                }

                _NotchedFontSize = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        float _SubThickness;
        public float SubThickness
        {
            get
            {
                return _SubThickness;
            }
            set
            {
                if (_SubThickness == value)
                {
                    return;
                }

                _SubThickness = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        LineType _MainLineType;
        public LineType MainLineType
        {
            get
            {
                return _MainLineType;
            }
            set
            {
                if (_MainLineType == value)
                {
                    return;
                }

                _MainLineType = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }

        }

        LineType _SubLineType;
        public LineType SubLineType
        {
            get
            {
                return _SubLineType;
            }
            set
            {
                if (_SubLineType == value)
                {
                    return;
                }

                _SubLineType = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, EventArgs.Empty);
                }
            }
        }

        public MainAxisData()
        {
            Title = new Text("", 8.0F);
            Digit = 2;
            Thickness = 2.0F;
            SubThickness = 1.0F;
            NotchedFontSize = 8.0F;
        }

        public void Auto(MaxMin data_max_min)
        {
            // a_1のnとa_nのmを算出する
            SignificantDigitReturn min = SignificantDigit(data_max_min.Min, Digit, true);
            SignificantDigitReturn max = SignificantDigit(data_max_min.Max, Digit, false);

            double x_min = min.real;
            double x_max = max.real;
            double W = x_max - x_min;
            SignificantDigitReturn w = SignificantDigit(W, Digit, false);
            double w_p = w.real;

            if (x_max > x_min)
            {
                x_min = x_max - w_p;
            }
            else
            {
                x_max = w_p + x_min;
            }

            // 切りが良くなるNを求める
            int N = DetectN((int)w.sig_int);

            // 以下，プロパティ設定になる
            // 切りが良くなるdeltaを求める
            Distance = (float)((x_max - x_min) / N);

            Min = (float)x_min;
            Max = (float)x_max;
        }

        static int DetectN(int p)
        {
            // 約数を求める
            List<int> r = new List<int>();
            for (int n = 1; (double)n <= Math.Sqrt(p); n++)
            {
                if (p % n == 0)
                {
                    r.Add(n);
                    int q = p / n;
                    if (n != q)
                    {
                        r.Add(q);
                    }
                }
            }

            r.Sort();   // これで約数の数列ができる

            if (r.Count % 2 == 0)
            {
                return r[r.Count / 2];
            }

            return r[(r.Count - 1) / 2];
        }

        public struct SignificantDigitReturn
        {
            public int power;
            public int sig_int;
            public double sig_real;
            public int sig_power;
            public double real;
        }

        static SignificantDigitReturn SignificantDigit(double a, int digit, bool use_floor)
        {
            SignificantDigitReturn ret = new SignificantDigitReturn();
            if (a == 0.0)
            {
                ret.sig_real = 0;
                ret.sig_power = 0;
                ret.sig_int = 0;
                ret.power = 0;
                return ret;
            }
            ret.power = (int)Math.Floor(Math.Log10(Math.Abs(a)));
            if (use_floor == false)
            {
                ret.sig_int = (int)Math.Ceiling(a / Math.Pow(10, ret.power - digit + 1));
            }
            else
            {
                ret.sig_int = (int)Math.Floor(a / Math.Pow(10, ret.power - digit + 1));
            }
            ret.sig_power = -digit + 1;
            ret.sig_real = ret.sig_int * Math.Pow(10, ret.sig_power);
            ret.real = ret.sig_int * Math.Pow(10, ret.power - digit + 1);
            return ret;
        }
    }
}
